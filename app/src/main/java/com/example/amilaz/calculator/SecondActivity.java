package com.example.amilaz.calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView resultTv;
    TextView resultBunbleTv;
    TextView resultParcleTv;
    TextView resultSerialTv;
    EditText sampleEditText;
    Button okBtn;
    double result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        InitInstance();
        AddListenner();
    }

    private void AddListenner() {
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", sampleEditText.getText().toString());
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    protected void InitInstance(){
        // Extra TV
        resultTv = (TextView) findViewById(R.id.textResult);
        result = getIntent().getExtras().getDouble("result");
        resultTv.setText("Result="+String.format("%.1f",result));
        // Bunble Extra
        resultBunbleTv = (TextView) findViewById(R.id.textResultBunble);
        Bundle bunble = getIntent().getExtras();
        Bundle c1 = bunble.getBundle("cBundle");
        resultBunbleTv.setText("From bundle: " + c1.getInt("x") + " " + c1.getInt("y") + " " + c1.getInt("z"));
        // Serializable
        resultSerialTv = (TextView) findViewById(R.id.textResultSerial);
        CoordinateSerializable c2 = (CoordinateSerializable) bunble.getSerializable("cSerializable");
        resultSerialTv.setText("From Serializable: " + c2.x + " " + c2.y + " " + c2.z);
        // Parcelable
        resultParcleTv = (TextView) findViewById(R.id.textResultParcelable);
        CoordinateParcelable c3 = bunble.getParcelable("cParcelable");
        resultParcleTv.setText("From Parcelable: " + c3.x + " " + c3.y + " " + c3.z);
        // Button Init
        okBtn = (Button) findViewById(R.id.okBtn);
        sampleEditText = (EditText) findViewById(R.id.editTextSeccond);
    }
}
