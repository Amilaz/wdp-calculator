package com.example.amilaz.calculator;

import android.content.Intent;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener {

    protected Point size;
    protected Display display;
    protected int screenWidth;
    protected int screeHeight;

    protected EditText input1Et;
    protected EditText input2Et;
    protected TextView output1Tv;

    protected Button calBtn;

    protected RadioGroup optRtnG;
    protected RadioButton plusRtn;
    protected RadioButton minusRtn;
    protected RadioButton multiRtn;
    protected RadioButton divideRtn;

    protected ProgressBar progress;

    protected Switch switchBtn;

    protected CustomViewGroup viewGroupHello;
    protected CustomViewGroup viewGroupWorld;

    protected long startTime;
    protected long endTime;

    protected String numberFormat = "Please only a number";
    protected String divideByZero = "Please divide by a non-zero number";

    double result = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeComponent();
        addListener();
    }

    private void initializeComponent() {
        // TextEdit in line 1
        input1Et = (EditText) findViewById(R.id.first_input);
        input2Et = (EditText) findViewById(R.id.second_input);
        // TextView in line 1
        output1Tv = (TextView) findViewById(R.id.output_cal);
        // Calculate Button
        calBtn = (Button) findViewById(R.id.cal_btn);
        // RadioButton
        optRtnG = (RadioGroup) findViewById(R.id.opt_rtng);
        plusRtn = (RadioButton) findViewById(R.id.opt_plus);
        minusRtn = (RadioButton) findViewById(R.id.opt_minus);
        multiRtn = (RadioButton) findViewById(R.id.opt_multi);
        divideRtn = (RadioButton) findViewById(R.id.opt_divide);
        // Progress Bar
        progress = (ProgressBar) findViewById(R.id.progress);
        // Switch Button
        switchBtn = (Switch) findViewById(R.id.sw);
        switchBtn.setText("OFF");
        //CustomViewGroup
        viewGroupHello = (CustomViewGroup) findViewById(R.id.vg1);
        viewGroupHello.setButtonText("Hello");
        viewGroupWorld = (CustomViewGroup) findViewById(R.id.vg2);
        viewGroupWorld.setButtonText("World");
    }

    private void addListener(){
        calBtn.setOnClickListener(this);
        optRtnG.setOnCheckedChangeListener(this);
        switchBtn.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == calBtn){
            startTime = System.currentTimeMillis();
            calculate(optRtnG.getCheckedRadioButtonId());
            getWindowSize();
            Intent i = new Intent(getApplicationContext(), SecondActivity.class);
            i.putExtra("result", result);
            // Test Bundle
            Coordinate c1 = new Coordinate();
            c1.x = 10;
            c1.y = 20;
            c1.z = 30;
            Bundle bunble = new Bundle();
            bunble.putInt("x", c1.x);
            bunble.putInt("y", c1.y);
            bunble.putInt("z", c1.z);
            i.putExtra("cBundle", bunble);
            // Test Serializable
            CoordinateSerializable c2 = new CoordinateSerializable();
            c2.x = 40;
            c2.y = 50;
            c2.z = 60;
            i.putExtra("cSerializable", c2);
            CoordinateParcelable c3 = new CoordinateParcelable();
            c3.x = 70;
            c3.y = 80;
            c3.z = 90;
            i.putExtra("cParcelable", c3);
            startActivityForResult(i, 12345);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(group == optRtnG){
            startTime = System.currentTimeMillis();
            calculate(checkedId);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView == switchBtn){
            startTime = System.currentTimeMillis();
            if (isChecked) {
                buttonView.setText("ON");
                progress.setIndeterminate(true);
            } else {
                buttonView.setText("OFF");
                progress.setIndeterminate(false);
            }
        }

    }

    private void calculate(int id){
        double[] input = new double[2];
        result = 0;

        acceptNumbers(input);

        switch (id){
            case R.id.opt_plus : result = input[0] + input[1]; break;
            case R.id.opt_minus : result = input[0] - input[1];break;
            case R.id.opt_multi : result = input[0] * input[1];break;
            case R.id.opt_divide :  if(input[1] != 0){
                                            result = input[1] / input[1];
                                    } else {
                                            showToast(divideByZero);
                                    }
                                    break;
            default: break;
        }
        output1Tv.setText(String.format("%.1f",result));
        endTime = System.currentTimeMillis();
        Log.d("Calculation", "computation time = " + (startTime - endTime / 1000.0));
    }

    private void showToast(String message){
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }

    private void acceptNumbers(double[] val){
        try{
            val[0] = Double.parseDouble(input1Et.getText().toString());
        } catch (NumberFormatException ex) {
            val[0] = 0;
            showToast(numberFormat);
        }

        try{
            val[1] = Double.parseDouble(input2Et.getText().toString());
        } catch (NumberFormatException ex) {
            val[1] = 0;
            showToast(numberFormat);
        }
    }

    private void getWindowSize(){
        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screeHeight = size.y;
        String showOnToast = "Width = " + screenWidth +", Height = " + screeHeight;
        showToast(showOnToast);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 12345){
            if(resultCode == RESULT_OK){
                showToast(data.getExtras().getString("result"));
            }
        }
    }

    /*@Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putString("op1", input1Et.getText().toString());
        outState.putString("op2", input2Et.getText().toString());
        outState.putString("result", output1Tv.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle saveInstanceState){
        input1Et.setText(saveInstanceState.getString("op1"));
        input2Et.setText(saveInstanceState.getString("op2"));
        output1Tv.setText(saveInstanceState.getString("result"));
    }*/

}
